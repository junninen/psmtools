function [ddat,dtm,dmf]=psm_inv_diff_minmax(tm,mf,d,mf_lim)
%
% Inversion by taking difference between minimum and maximum data 
% in between the flow limits.
%
% This inversion is ment to handle situation when during a scan a short high
% concentration blume occured and the concentration increas needs to
% be captured. Use raw, not cleaned data for that. 
%
% [ddat,dtm,dmf]=psm_inv_diff(tm,mf,dat,mf_lim)
%
% INPUTS:
%   tm  - (double, 1xm) vector of times for each measured flow
%   mf  - (double, size(tm)) vector of measured mixing flows
%   dat - (double, size(tm)) vector of measured concentration values
%   mf_lim - (double, 1xn) vector of flow limits to be defferenced
%             must be in ascending order
% OUTPUTS:
%   ddat - inverted data
%   dtm  - average time for the inverted data point
%   dmf  - average flows for the inverted data point

% Heikki Junninen
% Apr 2012

lm=length(mf_lim)-1;
ddat=NaN(1,lm);
dtm=ddat;
dmf=ddat;
for i=1:lm
    I=mf>mf_lim(i) & mf<=mf_lim(i+1);
    ddat(i)=max(d(:,I))-min(d(:,I));
    dtm(i)=mean(tm(:,I));
    dmf(i)=mean(mf(:,I));
end
