function psmD=psm_invert(psmD,varargin)
%
% Make inversion for PSM scans. Inversion is only applied to scans with all
% measurement points with status=1. Also additional mask filter can be given.
%
% psmD=psm_invert(psmD,[parNam,parVal...])
%
% INPUTS (in [] optional):
%   psmD - (struct)

%Heikki Junninen
%Apr 2012

meth='diff';
dataSet='raw';
mf_lims=[0.1:0.9/2:1];
calib_param=[-0.1252    0.2186]*1e-8;
loss_correction=1;
ok_lim=1;

%extracting inputs
i=1;
while i<=length(varargin),
    argok = 1;
    if ischar(varargin{i}),
        switch varargin{i},
            % argument IDs
            case 'method',            i=i+1; meth = varargin{i};
            case 'data_set',          i=i+1; dataSet = varargin{i};
            case 'mixFlow_lims',      i=i+1; mf_lims = varargin{i};
            case 'calib_param',       i=i+1; calib_param = varargin{i};
            case 'loss_correction',   i=i+1; loss_correction = varargin{i};
            case 'ok_lim',            i=i+1; ok_lim = varargin{i};
            otherwise
                argok=0;
        end
    end
    if ~argok,
        disp(['Ignoring invalid argument #' num2str(i+1)]);
    end
    i = i+1;
end

%select data
status_down=psmD.scans.down.status;
status_up=psmD.scans.up.status;

switch dataSet
    case 'clean'
        data_up=psmD.scans.up.conc_clean;
        mf_up=psmD.scans.up.mixFlow_clean;
        tm_up=psmD.scans.up.tim_clean;
        data_down=psmD.scans.down.conc_clean;
        mf_down=psmD.scans.down.mixFlow_clean;
        tm_down=psmD.scans.down.tim_clean;
    case 'raw'
        data_up=psmD.scans.up.conc;
        mf_up=psmD.scans.up.mixFlow;
        tm_up=psmD.scans.up.tim;
        data_down=psmD.scans.down.conc;
        mf_down=psmD.scans.down.mixFlow;
        tm_down=psmD.scans.down.tim;
    otherwise
        disp([mfilename, ': Unknown data_set'])
end

% inversion
nr_mf_lims=length(mf_lims);
nr_mf=size(mf_down,2);
if nr_mf_lims>nr_mf
    disp([mfilename, ': number of flow limits too high. '])
    return
end

ddat_down=NaN(size(data_down,1),nr_mf_lims-1);
ddat_up=ddat_down;
dtm_down=ddat_down;
dtm_up=ddat_down;
dmf_down=ddat_down;
dmf_up=ddat_down;
mf=NaN(1,nr_mf_lims-1);
nr_steps=length(psmD.scans.up.status(1,:));
switch meth
    case 'diff'
        %difference of data points closest the flow limits
        mf_vec=0;
        if size(mf_down,1)==1
            mf_vec=1;
        end
        
        %down
        for i=1:size(data_down,1)
            oks=sum(status_down(i,:));
            if ~all(isnan(data_down(i,:))) & ok_lim<=oks/nr_steps
                if mf_vec
                    [ddat_down(i,:),dtm_down(i,:)]=psm_inv_diff(tm_down(i,:),mf_down(1,:),data_down(i,:),mf_lims);
                else
                    [ddat_down(i,:),dtm_down(i,:)]=psm_inv_diff(tm_down(i,:),mf_down(i,:),data_down(i,:),mf_lims);
                end
            end
        end
        
        %up
        for i=1:size(data_up,1)
            oks=sum(status_up(i,:));
            if ~all(isnan(data_up(i,:))) & ok_lim<=oks/nr_steps
                if mf_vec
                    [ddat_up(i,:),dtm_up(i,:)]=psm_inv_diff(tm_up(i,:),mf_up(1,:),data_up(i,:),mf_lims);
                else
                    [ddat_up(i,:),dtm_up(i,:)]=psm_inv_diff(tm_up(i,:),mf_up(i,:),data_up(i,:),mf_lims);
                end
            end
        end
        for i=1:length(mf_lims)-1
            dmf(:,i)=(mf_lims(:,i)+mf_lims(:,i+1))/2;
        end
        
        dtm_up(dtm_up==0)=NaN;
        dtm_down(dtm_down==0)=NaN;
        psmD.inverted.dp_avr=psm_flow2size(dmf,calib_param);
        psmD.inverted.losses=psm_diffusion_losses(psmD.inverted.dp_avr);
        psmD.inverted.loss_correction=loss_correction;
        
        
        psmD.inverted.mixFlow_lims=mf_lims;
        psmD.inverted.mixFlow_avr=dmf;
        psmD.inverted.down.data=ddat_down;
        psmD.inverted.down.tim=dtm_down;
        psmD.inverted.up.data=ddat_up;
        psmD.inverted.up.tim=dtm_up;
        
    case 'diff_minmax'
        % difference between minimum and maximum data in between the flow
        % limits
        mf_vec=0;
        if size(mf_down,1)==1
            mf_vec=1;
        end
        
        %down
        for i=1:size(data_down,1)
            oks=sum(status_down(i,:));
            if ~all(isnan(data_down(i,:))) & ok_lim<=oks/nr_steps
                if mf_vec
                    [ddat_down(i,:),dtm_down(i,:)]=psm_inv_diff_minmax(tm_down(i,:),mf_down(1,:),data_down(i,:),mf_lims);
                else
                    [ddat_down(i,:),dtm_down(i,:)]=psm_inv_diff_minmax(tm_down(i,:),mf_down(i,:),data_down(i,:),mf_lims);
                end
            end
        end
        
        %up
        for i=1:size(data_up,1)
            oks=sum(status_up(i,:));
            
            if ~all(isnan(data_up(i,:))) & ok_lim<=oks/nr_steps
                if mf_vec
                    [ddat_up(i,:),dtm_up(i,:)]=psm_inv_diff_minmax(tm_up(i,:),mf_up(1,:),data_up(i,:),mf_lims);
                else
                    [ddat_up(i,:),dtm_up(i,:)]=psm_inv_diff_minmax(tm_up(i,:),mf_up(i,:),data_up(i,:),mf_lims);
                end
            end
        end
        for i=1:length(mf_lims)-1
            dmf(:,i)=(mf_lims(:,i)+mf_lims(:,i+1))/2;
        end
        
        dtm_up(dtm_up==0)=NaN;
        dtm_down(dtm_down==0)=NaN;
        
        
        psmD.inverted.mixFlow_lims=mf_lims;
        psmD.inverted.mixFlow_avr=dmf;
        psmD.inverted.dp_avr=psm_flow2size(dmf,calib_param);
        psmD.inverted.losses=psm_diffusion_losses(psmD.inverted.dp_avr);
        psmD.inverted.loss_correction=loss_correction;
        psmD.inverted.down.data=ddat_down;
        psmD.inverted.down.tim=dtm_down;
        psmD.inverted.up.data=ddat_up;
        psmD.inverted.up.tim=dtm_up;
    case 'lsq'
        disp([mfilename, ': inversion method "',meth,'" not implemented '])
        
    case 'pmf'
        disp([mfilename, ': inversion method "',meth,'" not implemented '])
    otherwise
        disp([mfilename, ': Unknown method'])
end

if loss_correction
    corr=repmat(1+(1-psmD.inverted.losses),size(psmD.inverted.down.data,1),1);
    psmD.inverted.down.data= psmD.inverted.down.data.*corr;
    corr=repmat(1+(1-psmD.inverted.losses),size(psmD.inverted.up.data,1),1);
    psmD.inverted.up.data=psmD.inverted.up.data.*corr;
end
