function yy = interpmon(x,y,xx)

% Monotone cubic interpolation using Fritsch-Carlson method.
% See: http://en.wikipedia.org/wiki/Monotone_cubic_interpolation
%
% Input:
%   x  - x data (must be monotonic)
%   y  - y data (if not monotonic, interpolation will not be either)
%   xx - x axis to interpolate to
%
% Output:
%   yy - interpolated data
%

% Gustaf L�nn
% December 2011

x = x(:)';
y = y(:)';
xx = xx(:)';
n = length(y);
m = zeros(1,n);
yy = zeros(1,length(xx));
alpha = zeros(1,n-1);
beta = zeros(1,n-1);
tau = zeros(1,n-1);

if length(x) ~= length(y)
    error('interpmon: x and y must be of the same length.');
end

if n < 2
    error('interpmon: At least two data points needed.');
end

% preprocessing

delta = diff(y)./diff(x);
m(1) = delta(1);
m(n) = delta(n-1);
m(2:end-1) = (delta(1:end-1) + delta(2:end)) / 2;

dZero = delta == 0;
shiftdZero = [false dZero];
shiftnotdZero = ~shiftdZero;
shiftnotdZero(1) = false;
notdZero = ~dZero;
m(dZero) = 0;
m(shiftdZero) = 0;
alpha(notdZero) = m(notdZero) ./ delta(notdZero);
beta(notdZero) = m(shiftnotdZero) ./ delta(notdZero);

notMonotonic = alpha < 0 | beta < 0;
shiftnotMonotonic = [false notMonotonic];
m(notMonotonic) = 0;
m(shiftnotMonotonic) = 0;
alpha(notMonotonic) = 0;
beta(notMonotonic) = 0;

overshoot = alpha.^2 + beta.^2 > 9;
shiftovershoot = [false overshoot];
tau(overshoot) = 3 ./ sqrt(alpha(overshoot).^2 + beta(overshoot).^2);
m(overshoot) = tau(overshoot) .* alpha(overshoot) .* delta(overshoot);
m(shiftovershoot) = tau(overshoot) .* beta(overshoot) .* delta(overshoot);

% interpolation

for i = 1:length(xx)
    Isel = x(1:end-1) <= xx(i) & xx(i) <= x(2:end); % VERY SLOW!
    Isel = find(Isel,1);
    if isempty(Isel)
        yy(i) = NaN;
    else
        h = x(Isel + 1) - x(Isel);
        t = (xx(i) - x(Isel)) ./ h;
        yy(i) = y(Isel) * (2*t^3 - 3*t^2 + 1) + h * m(Isel) * (t^3-2*t^2+t) + y(Isel + 1) * (-2*t^3+3*t^2) + h * m(Isel + 1) * (t^3 - t^2);
    end
end