function yout = pav(yin)

% Makes data monotonic using the PAV (pool-adjacent-violators) algorithm.
% yout = pav(yin)
%
% Input:
%   yin - input data
%
% Output:
%   yout - monotonic data
%

% Gustaf L�nn
% December 2011

yout = yin;

for i = 1:length(yout) - 1
    if yout(i) > yout(i + 1)
        yout(i) = (yout(i) + yout(i + 1)) /2;
        yout(i + 1) = yout(i);
        a = 1;
        if i - a ~= 0
            while yout(i - a) > yout(i)
                yout(i-a:i+1) = sum(yout(i-a:i+1)) ./ (a + 2);
                a = a + 1;
                if i - a == 0
                    break;
                end
            end
        end
    end
end