function H_progessBar_ascii(done_frac,fun)
%
% ascii progress bar for command line
%
% H_progessBar_ascii(done_frac,fun)
%
% done_frac - fraction of a process that is completed
% fun       - string to be displayed. 
%
% EXAMPLE:
% show progress bar with function name that called the bar
% for i=1:100
%     % some process
%     H_progessBar_ascii(i/100,mfilename)
% end
%

%Heikki Junninen
%Apr 2012

persistent done_progress
maxNrChar=75;

done_progress=floor(maxNrChar*done_frac);

clc,
disp(sprintf('%s: %3.0f%% done.',fun,done_frac*100))
disp([repmat('|',1,1),repmat('+',1,done_progress),repmat('-',1,maxNrChar-done_progress),repmat('|',1,1)])
if done_frac==1
    done_progress=0;
end
