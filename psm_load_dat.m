function psmD=psm_load_dat(fNams,varargin)
%
% Read raw PSM data file
%
% psmD=psm_load_dat(fNam)
%
% INPUTS:
%   fNam      - (char) file name of raw PSM data file (with path)
%               (cell) cell with multiple file names,
%               collect all and sort by time
%   time_lag  - (scalar) time lag between particle sampling and
%               detecting in seconds (default,5s)
%   coin_corr - (logical) coincidence correction for TSI 3010,
%               1-do, 0-dont (default, 0)
%   dilution_corr - if CPC has been diluted, give dilution factor, default=1
%               dilution_factor = (cpc_flow+dilution_flow)/cpc_flow
%
% OUTPUTS:
%   psmD - (struct) structure that collects the data and analysis results
%        .tim     - time vector in datenum - format
%        .conc    - particle counts [cm-3]
%        .err     - error from counting statistics
%        .mixFlow - measured mixing flow rate
%        .status  - status reported by the instrument
%

% Heikki Junninen
% Apr 2012

ver=psm_get_version;
disp(sprintf('psmTools revision %d',ver))

time_lag=3; %s
coin_corr=false;
inlet_flow=2.5; %lpm
dilution_corr=1;

psmD=struct('tim',[],'tim_dt',[],'conc',[],'err',[],'mixFlow',[],'status',[]);

%extracting inputs
i=1;
while i<=length(varargin),
    argok = 1;
    if ischar(varargin{i}),
        switch varargin{i},
            % argument IDs
            case 'time_lag',           i=i+1; time_lag = varargin{i};
            case 'coin_corr',         i=i+1; coin_corr = varargin{i};
            case 'dilution_corr' ,    i=i+1; dilution_corr = varargin{i};
            otherwise
                argok=0;
        end
    end
    if ~argok,
        disp(['Ignoring invalid argument #' num2str(i+1)]);
    end
    i = i+1;
end


if ischar(fNams)
    fNams={fNams};
end
nrF=length(fNams);


%-------------------------------------------------------------------------
for f=1:nrF
    fNam=fNams{f};
    isA11=0;
    if exist(fNam,'file')
        
        fid=fopen(fNam);
        if contains(fgetl(fid),'A11')
            isA11=1;
        end
        fclose(fid);
        
        if isA11
            C=psm_load_dat_A11(fNam);
        else
            
            fid=fopen(fNam);
            C = textscan(fid, '%s%f%f%d','delimiter',',');
            fclose(fid);
        end
        %load parameter file
        %     fid=fopen(fNamPar);
        %     C_par = textscan(fid, '%s%f%f%d','delimiter',',');
        %     fclose(fid);
    else
        disp(['psm_load_dat: file ',fNam,' not find!'])
        break
    end
    
    if isA11
        conc=C{2};
        mixFlow=single(C{4});
        status=uint8(C{15});
        tim=datenum(C{1}); %datenum format
        tim_dt=C{1}; %datetime format
    else
        conc=C{2};
        mixFlow=single(C{3});
        status=uint8(C{4});
        
        timStr=cat(1,C{1}{:});
        timNum=NaN(size(timStr,1),6);
        
        %convert time
        for i=1:size(timStr,1),timNum(i,:)=sscanf(timStr(i,:),'%d.%d.%d %d:%d:%d')';end
        tim=datenum(timNum(:,[3,2,1,4,5,6]));
    end
    
    psmD.tim=[psmD.tim;tim];
    psmD.conc=[psmD.conc;conc];
    psmD.mixFlow=[psmD.mixFlow;mixFlow];
    psmD.status=[psmD.status;status];
    if isA11
        psmD.tim_dt=[psmD.tim_dt;tim_dt];
    end
end
psmD.revision=ver;

%sort by time
[~,Is]=sort(psmD.tim,'ascend');
psmD.tim=psmD.tim(Is);
psmD.conc=psmD.conc(Is);
psmD.mixFlow=psmD.mixFlow(Is);
psmD.status=psmD.status(Is);

% time lag correction

tm_stp=median(diff(psmD.tim))*24*60*60;
stps=round(time_lag/tm_stp);
if stps>0
    psmD.tim=psmD.tim(1:end-stps);
    psmD.mixFlow=psmD.mixFlow(1:end-stps);
    psmD.status=psmD.status(1:end-stps);
    psmD.conc=psmD.conc((stps+1):end);
end


%calculate error estimate from counting statistics
sf=inlet_flow-psmD.mixFlow; %l/min; actual sample flow used for analysis
counts=psmD.conc*1000.*sf/60*tm_stp; %counts in sampling time interval
psmD.err=(sqrt(counts)/tm_stp*60)./(sf*1000);

% coincidence correction
if coin_corr
    c3010=psmD.conc.*(inlet_flow)./(inlet_flow+psmD.mixFlow);
    psmD.conc=psmD.conc.*exp(c3010*16.67*0.4e-6);
    psmD.err=psmD.err.*exp(c3010*16.67*0.4e-6);
end

% dilution correction
psmD.conc=psmD.conc*dilution_corr;
psmD.err=psmD.err*dilution_corr;