function fileList = psm_generate_file_list(path,startTime,stopTime)
%
% Generate file list for psm_load-function 
%
% fileList = psm_generate_file_list(path,startTime,stopTime)
%
% path - path for data files
% startTime - start time in datenum
% stopTime  - stop time in datenum

% Gustaf L�nn, Heikki Junninen
% May 2012

if path(end) ~= filesep
    path(end + 1) = filesep;
end
fileInfo = dir([path '*.dat']);

% fileStrDates = cellfun(@(x)regexp(x,'[0-9]{10,10}','match'),{fileInfo.name},'UniformOutput',false);
fileStrDates = cellfun(@(x)regexp(x,'[0-9]{8}','match'),{fileInfo.name},'UniformOutput',false);
badFile = cellfun(@isempty,fileStrDates);
fileInfo(badFile) = [];
fileStrDates(badFile) = [];
fileStrDates = [fileStrDates{:}];
% fileDates = cellfun(@(x)datenum(x,'yyyymmddHH'),fileStrDates);
fileDates = cellfun(@(x)datenum(x,'yyyymmdd'),fileStrDates);
goodFile = fileDates >= startTime & fileDates <= stopTime;
fileList = {fileInfo(goodFile).name};
fileList = cellfun(@(x)[path x],fileList,'UniformOutput',false);

if isempty(fileList)
disp('no files')
end