function [conc,conc_up,conc_down,common_tm]=psm_time_avr(psmD,avr_time)
%
% Calculate time average of inverted PSM data.
%
% [conc,conc_up,conc_down,common_tm]=psm_tim_avr(psmD,avr_time)
%
% INPUTS:
%   psmD  - (struct) psm-structure
%   avr_time - two ways of giving a average time
%            - (scalar, minutes) time window for averageing,
%              first averaged data value will be at +-1/2 avr_time
%            - (double, nx1) time vector to calculate the average. Given
%            time points will be at meddle of the average window
% OUTPUTS:
%   conc      - Averaged concentration of both up and down scans
%   conc_up   - Averaged concentration of up scans
%   conc_down - Averaged concentration of down scans
%   common_tm - common time axes
%
 
%Heikki Junninen
% Apr 2012

if length(avr_time)==1
    %construct averaging time vector
    stp=avr_time/(60*24) %days
    common_tm=[min(psmD.tim):stp:max(psmD.tim)]';
else
    stp=median(abs(diff(avr_time))); %days
    common_tm=avr_time;
end


%take the time for a scan at the middle of the scan
scanSteps=size(psmD.inverted.mixFlow_avr,2);

Itm=round(scanSteps/2);
conc_down=NaN(length(common_tm),length(psmD.inverted.down.tim(1,:)));
conc_up=conc_down;

for i=1:length(common_tm)
    
    Isel=psmD.inverted.down.tim(:,Itm)>common_tm(i)-stp/2 & psmD.inverted.down.tim(:,Itm)<=common_tm(i)+stp/2;
    conc_down(i,:)=nanmean(psmD.inverted.down.data(Isel,:),1);
    
    Isel=psmD.inverted.up.tim(:,Itm)>common_tm(i)-stp/2 & psmD.inverted.up.tim(:,Itm)<=common_tm(i)+stp/2;
    conc_up(i,:)=nanmean(psmD.inverted.up.data(Isel,:),1);
    
end
c1=conc_down;
c2=conc_up;

c1(isnan(c1))=0;
c2(isnan(c2))=0;
conc=c1+c2;
clear c1 c2

oks=~isnan(conc_down)+~isnan(conc_up);
conc=conc./oks;