function [psm,Cok]=psm_exorcist(fNam)
%
% Read in the PSM (A10) data and fix the up-side scans
% It assumes
%
% return original data and corrected concentration
%

%Heikki Junninen
%Mar 2014



psm=importdata(fNam);
%psm=importdata('/Users/junninen/Documents/1h/campaigns/JFJ2014/PSM/data/CPC_WARMUP_20140218.dat');
tm=datenum(psm.textdata,'dd.mm.yyyy HH:MM:SS');
Fe=psm.data(:,3);
Fs=psm.data(:,2);
Fcpc=psm.data(:,13);
Fcpc=-0.1556*Fs+0.9656; %CPC inlet flow depends on saturation flow
P=psm.data(:,10);
T=psm.data(:,9)+273.15;

T0=21.11+273.15; %K
P0=101.325; %kPa

% Ts_dif=35.5;
% Te_dif=10.27;

%Ts_dif=-5;
%Te_dif=0;

% Fs_v=Fs.*((T+Ts_dif)./T0).*(P0./P);
% Fe_v=Fe.*((T+Te_dif)./T0).*(P0./P);

Fs_v=Fs.*((min(T,25+273)+140)./T0).*(P0./P);
Fe_v=Fe.*((3+273.15)./T0).*(P0./P);

Fi=Fe_v+Fcpc-Fs_v;
Fi_old=Fe+Fcpc-Fs;

Corig=psm.data(:,1);

Ccpc=Corig.*(Fi_old)./(Fi_old+Fs);

Cok=Ccpc.*(Fi+Fs_v)./Fi;

% figure,plot([Corig,Cok])