function psmD=psm_clean(psmD,varargin)
%
% Clean scans by removing spikes, make them monotonic and interpolate to a
% given flow-space. Cleaning is only done to scans that have unique status
% value.
% Required preceding steps are:
% psmD=psm_load_dat(fNam_psm);
% psmD=psm_fold2scans(psmD);
%
%
% psmD=psm_clean(psmD,[parNam,parVal...])
%
% INPUTS (in [] optional):
%   psmD  - (struct) psm-structure
%
%   OPTIONAL - parameters are given as parameter name and value pairs
%   interp_flows    - (vector, 1 x m) flow values data will be interpolated to
%   remove_win   - (value) window for the median filter (default 7)
%   remove_limit - (value) Residual values above limit are removed one by one.
%                  Residual is difference between median filtered data and data.
%                  Median filter data is only used for spike removal, not
%                  for any further analysis. (default 60)
%   doPlot       - (logical value) if 1 plots each scan,
%                  if 0 no plotting (default, faster)
%   ok_lim       - (scalar) limit of scanning point that have to be 1. Range 0-1 
%                  (default 1)
%
% OUTPUTS:
%   psmD - (struct) cleaned, monotonic scans appended to the struct
%       .scans.up.conc_clean - (double, n x m, n - time points, m - length(interp_flows ))
%       .scans.up.mixFlow_clean - (double, 1 x m ) - interp_flows
%       .scans.up.tim_clean - (double, n xm) - time point closest to
%       interpolated flow points.
%

%Heikki Junninen
%Apr 2012

lim=60;
stp=7;
doPlot=0;

maxF=1;
minF=0.1;
nrS=24;
flow_points=minF:(maxF-minF)/(nrS-1):maxF;
ok_lim=1;
%extracting inputs
i=1;
while i<=length(varargin),
    argok = 1;
    if ischar(varargin{i}),
        switch varargin{i},
            % argument IDs
            case 'remove_win',           i=i+1; stp = varargin{i};
            case 'remove_limit',         i=i+1; lim = varargin{i};
            case 'doPlot',               i=i+1; doPlot = varargin{i};
            case 'status',               i=i+1; status = varargin{i};
            case 'interp_flows',         i=i+1; flow_points = varargin{i};
            case 'ok_lim',               i=i+1; ok_lim = varargin{i};
                
            otherwise
                argok=0;
        end
    end
    if ~argok,
        disp(['Ignoring invalid argument #' num2str(i+1)]);
    end
    i = i+1;
end

%-------------------------------------------------------------------------

nrS=length(flow_points);
%down scans
nrScans=size(psmD.scans.down.mixFlow,1);
d2_df_down=NaN(nrScans,nrS);
tm_down=d2_df_down;
nr_steps=length(psmD.scans.up.status(1,:));
for i=1:nrScans
    d2=psmD.scans.down.conc(i,:);
    f2=psmD.scans.down.mixFlow(i,:);
    tm=psmD.scans.down.tim(i,:);
    stat=psmD.scans.down.status(i,:);
    oks=sum(stat==1); %number of ok status measurements, currently only for status value 1
    if ok_lim<=oks/nr_steps
        [d2_df_down(i,:)]=psm_clean_scan(d2,f2,flow_points,'remove_limit',lim,'remove_win',stp,'doPlot',doPlot);
        Itm=NaN(1,nrS);
        for ii=1:nrS
            [~,I]=min(abs(f2-flow_points(ii)));
            Itm(ii)=I;
        end
        tm_down(i,:)=tm(Itm);
    end
    H_progessBar_ascii(i/nrScans,[mfilename,': down scans'])
end

%up scans
nrScans=size(psmD.scans.up.mixFlow,1);
d2_df_up=NaN(nrScans,nrS);
tm_up=d2_df_up;
for i=1:nrScans
    d2=psmD.scans.up.conc(i,:);
    f2=psmD.scans.up.mixFlow(i,:);
    tm=psmD.scans.up.tim(i,:);
    stat=psmD.scans.up.status(i,:);
    oks=sum(stat==1); %number of ok status measurements, only value 1
    if ok_lim<=oks/nr_steps
        
        [d2_df_up(i,:)]=psm_clean_scan(d2,f2,flow_points,'remove_limit',lim,'remove_win',stp,'doPlot',doPlot);
        %match time, closest time point to measured flows to flow_points
        Itm=NaN(1,nrS);
        for ii=1:nrS
            [~,I]=min(abs(f2-flow_points(ii)));
            Itm(ii)=I;
        end
        tm_up(i,:)=tm(Itm);
    end
    H_progessBar_ascii(i/nrScans,[mfilename,': up scans'])
end

%save to struct
psmD.scans.down.mixFlow_clean=flow_points(:)';
psmD.scans.down.conc_clean=d2_df_down;
psmD.scans.down.tim_clean=tm_down;
psmD.scans.up.mixFlow_clean=flow_points(:)';
psmD.scans.up.conc_clean=d2_df_up;
psmD.scans.up.tim_clean=tm_up;