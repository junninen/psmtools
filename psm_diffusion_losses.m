function [loss]=psm_diffusion_losses(dp)
%
% Estimate diffusion losses in inlet tube for given sizes
% 
% inlet_flow=2.5; %lpm
% tube_diam=6e-3; %m
% tube_length=0.3; %m
% T_K=20+273;%K
% P=101325;%Pa
% density=100 kg/m3

tra=load('tube_losses.mat');
loss=NaN(size(dp));
mn=min(tra.dp);
mx=max(tra.dp);
for i=1:length(dp)
    if dp(i)>mx | dp(i)<mn
        disp([mfilename,': size ',num2str(dp(i)),' outside precalculated range (',num2str(min(tra.dp)),' - ',num2str(max(tra.dp)),')'])
    end
    [~,Imn]=min(abs(tra.dp-dp(i)));
    loss(i)=1-tra.transmission(Imn);
end