function psmD=psm_fold2scans(psmD,varargin)
%
% Fold time series of PSM scans to matrix of time vs mixing flow.
% Separate up-scans and down-scans
%
% psmD=psm_fold2scans(psmD,[parNam,parVal...])
%
% INPUTS (in [] optional):
%   psmD - (struct) structure saved out form the psm_load_dat
%
%   OPTIONAL - parameters are given as parameter name and value pairs
%   keep_raw   - (logical) set 0 if want to remove original raw data
%                  from the structure, set to 1 if want to keep it (default,1)
%   scan_steps  - (scalar) number of steps/scan
%
%
% OUTPUTS:
%   psmD - (struct) structure appended with data folded to scan matrix
%       .scans.up.conc - (double, n x m, n-time points, m-mixing flow points)
%                        counts per mixing flow setting
%       .scans.up.err - (single, size(.conc)) error from counting
%       .scans.up.mixFlow - (single, size(.conc)) mixing flows
%       .scans.up.tim - (single, size(.conc)) time points
%       .scans.up.status - (uint8, size(.conc)) status value for each point
%
%       and same for down scans
%
% EXAMPLE:
%   Fold data to scans that are 120 steps long
%   psmD=psm_fold2scans(psmD,'scan_steps',120);
%

%Heikki Junninen
% Apr 2012

%defaults
keep_raw=1;
scanSteps=120;

%extracting inputs
i=1;
while i<=length(varargin),
    argok = 1;
    if ischar(varargin{i}),
        switch varargin{i},
            % argument IDs
            case 'keep_raw',           i=i+1; keep_raw = varargin{i};
            case 'scan_steps',          i=i+1; scanSteps = varargin{i};
            case 'scan_step_tim',        i=i+1; scanStepTim = varargin{i};
            otherwise
                argok=0;
        end
    end
    if ~argok,
        disp(['Ignoring invalid argument #' num2str(i+1)]);
    end
    i = i+1;
end

disp(sprintf('%s: Using %d steps per scan',mfilename,scanSteps))
%% find max mixingFlows points

Imx=psm_find_scan_maxs(psmD.mixFlow,psmD.status,scanSteps);

%% fold the data
psm_scans_down_c=NaN(length(Imx),scanSteps);
psm_scans_up_c=NaN(length(Imx),scanSteps);
psm_scans_down_f=NaN(length(Imx),scanSteps,'single');
psm_scans_up_f=NaN(length(Imx),scanSteps,'single');
psm_scans_down_tim=psm_scans_up_c;
psm_scans_up_tim=psm_scans_up_c;
psm_scans_up_err=psm_scans_up_c;
psm_scans_down_err=psm_scans_up_c;
psm_scans_down_status=uint8(psm_scans_up_f);
psm_scans_up_status=uint8(psm_scans_up_f);
for i=1:length(Imx)
    %collect only full scans
    if Imx(i)+scanSteps-1<length(psmD.conc)
        psm_scans_down_c(i,:)=psmD.conc(Imx(i):Imx(i)+scanSteps-1);
        psm_scans_down_f(i,:)=psmD.mixFlow(Imx(i):Imx(i)+scanSteps-1);
        psm_scans_down_tim(i,:)=psmD.tim(Imx(i):Imx(i)+scanSteps-1);
        psm_scans_down_status(i,:)=psmD.status(Imx(i):Imx(i)+scanSteps-1);
        psm_scans_down_err(i,:)=psmD.err(Imx(i):Imx(i)+scanSteps-1);
    end
    
    if Imx(i)-scanSteps+1>0
        psm_scans_up_c(i,:)=psmD.conc(Imx(i)-scanSteps+1:Imx(i));
        psm_scans_up_f(i,:)=psmD.mixFlow(Imx(i)-scanSteps+1:Imx(i));
        psm_scans_up_tim(i,:)=psmD.tim(Imx(i)-scanSteps+1:Imx(i));
        psm_scans_up_status(i,:)=psmD.status(Imx(i)-scanSteps+1:Imx(i));
        psm_scans_up_err(i,:)=psmD.err(Imx(i)-scanSteps+1:Imx(i));
    end
end

% save to struct
if ~keep_raw
    psmD=rmfield(psmD,'conc');
    psmD=rmfield(psmD,'mixFlow');
end

psmD.scans.up.conc=psm_scans_up_c;
psmD.scans.up.err=psm_scans_up_err;
psmD.scans.up.mixFlow=psm_scans_up_f;
psmD.scans.up.tim=psm_scans_up_tim;
psmD.scans.up.status=psm_scans_up_status;

psmD.scans.down.conc=psm_scans_down_c;
psmD.scans.down.err=psm_scans_down_err;
psmD.scans.down.mixFlow=psm_scans_down_f;
psmD.scans.down.tim=psm_scans_down_tim;
psmD.scans.down.status=psm_scans_down_status;

% clean bad scans out
difLim=0.1;
Ibad=any(abs(diff(psmD.scans.up.mixFlow,1))>difLim,2);
psmD.scans.up.conc(Ibad,:)=[];
psmD.scans.up.err(Ibad,:)=[];
psmD.scans.up.mixFlow(Ibad,:)=[];
psmD.scans.up.tim(Ibad,:)=[];
psmD.scans.up.status(Ibad,:)=[];

Ibad=any(abs(diff(psmD.scans.down.mixFlow,1))>difLim,2);
psmD.scans.down.conc(Ibad,:)=[];
psmD.scans.down.err(Ibad,:)=[];
psmD.scans.down.mixFlow(Ibad,:)=[];
psmD.scans.down.tim(Ibad,:)=[];
psmD.scans.down.status(Ibad,:)=[];
