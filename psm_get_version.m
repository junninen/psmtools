function ver=psm_get_version
%
% Get the current revision of the working copy
%

%Heikki Junninen
%Apr 2011

p=which(mfilename);
[pathF,~,~]=fileparts(p);
cdNow=cd;
cd(pathF)

if ispc
    try
        [~,~]=system(['SubWCRev.exe ',pathF,' version_template.txt version.h']);
    catch
        %disp(E.message)
    end
end
cd(cdNow);

% print the content
sep=filesep;
fid=fopen([pathF,sep,'version.h']);
h=0;
while 1
    h=h+1;
    tline=fgetl(fid);
    if tline==-1,break,end
    if h==1
        ver=str2num(tline(10:end));
    end
    %disp(tline)
end
fclose(fid);
