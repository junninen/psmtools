function [ddat,dtm,dmf]=psm_inv_diff(tm,mf,d,mf_lim)
%
% Inversion by taking difference between flow points
%
% [ddat,dtm,dmf]=psm_inv_diff(tm,mf,dat,mf_lim)
%
% INPUTS:
%   tm  - (double, 1xm) vector of times for each measured flow
%   mf  - (double, size(tm)) vector of measured mixing flows
%   dat - (double, size(tm)) vector of measured concentration values
%   mf_lim - (double, 1xn) vector of flow limits to be defferenced
%             must be in ascending order
% OUTPUTS:
%   ddat - inverted data
%   dtm  - average time for the inverted data point
%   dmf  - average flows for the inverted data point

% Heikki Junninen
% Apr 2012

lm=length(mf_lim)-1;
ddat(1,lm)=NaN;
dtm(1,lm)=NaN;
dmf(1,lm)=NaN;
for i=1:lm
    [~,Imn1]=min(abs(mf-mf_lim(i)));
    [~,Imn2]=min(abs(mf-mf_lim(i+1)));
    ddat(i)=d(Imn2)-d(Imn1);
    dtm(i)=(tm(Imn2)+tm(Imn1))/2;
    dmf(i)=(mf(Imn2)+mf(Imn1))/2;
end
