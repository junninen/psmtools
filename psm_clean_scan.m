function [d2_df,f_df]=psm_clean_scan(d2,f2,f_df,varargin)
%
% Clean PSM scans by removing spikes, make monotonic by PAV
% (pool-adjacent-violators) algorithm, and interpolate  to given flow-space
% (monotone cubic interpolation using Fritsch-Carlson method)
%
% [counts]=psm_clean(measured_counts,measured_flows,interp_flow,[parNam,parVal...])
%
% INPUTS (in [] optional):
%   measured_counts - (vector, [1xn], n-scanSteps) measured particle counts
%   measured_flows  - (vector, size(measured_counts)) measured mixing flows
%   interp_flows    - (vector, [1xm]) flow values data will be interpolated to
%
%   OPTIONAL - parameters are given as parameter name and value pairs
%   remove_win   - (value) window for the median filter (default 7)
%   remove_limit - (value) relative residual values above limit are removed one by one.
%                  Residual is difference between median filtered data and data divided by data.
%                  Median filter data is only used for spike removal, not
%                  for any further analysis. (default 60)
%   doPlot       - (logical value) if 1 plots each scan, 
%                  if 0 no plotting (default, faster) 
%
% OUTPUTS:
%   counts - (vector, size(interp_flows)) cleaned, monotonic scan
%

% Heikki Junninen
% Apr 2012

lim=60;
stp=7;
doPlot=0;

%extracting inputs
i=1;
while i<=length(varargin),
	argok = 1;
	if ischar(varargin{i}),
		switch varargin{i},
			% argument IDs
			case 'remove_win',           i=i+1; stp = varargin{i};
			case 'remove_limit',         i=i+1; lim = varargin{i};
			case 'doPlot',               i=i+1; doPlot = varargin{i};
				
            otherwise
                argok=0;
        end
	end
	if ~argok,
		disp(['Ignoring invalid argument #' num2str(i+1)]);
	end
	i = i+1;
end


%----------------------------------------------------------------------
if doPlot
    figure(112),
    clf,
    subplot(2,2,1)
    plot([d2;H_rm_spikes(d2,stp)]')
    subplot(2,2,2)
    plot([(d2-H_rm_spikes(d2,stp))./H_rm_spikes(d2,stp)]')
    ylim([-lim/2 lim*1.3])
    line([1 120],[lim lim])
    subplot(2,2,4)
    plot(f2,d2)
end

tol=inf;
iter=0;
while tol>lim
    iter=iter+1;
    res=d2-H_rm_spikes(d2,stp);
    %use only values more than remove_win away from start or end, since 
    %there the filter is not working properly
    res(1:stp)=0;
    res(end-stp:end)=0;
    
    [tol,Imx]=max(res./d2);
    
    if doPlot
        subplot(2,2,3),
        plot(res./d2),
    end
    if max(f2)~=f2(Imx)
        d2(Imx)=[];
        f2(Imx)=[];
    else
        [s,Is]=sort(res,'descend');
        Imx=Is(2);
        tol=s(2);
        
        d2(Imx)=[];
        f2(Imx)=[];
    end
    if length(d2)<=stp+2
        tol=0;
        d2=NaN(size(f2));
        disp('no good data')
    end
    
end
if doPlot
    subplot(2,2,4)
    hold on
    plot(f2,d2,'o')
    title(iter)
end
%
%  f_lims=[0.1:0.9/7:1];
% d2_df=NaN(1,length(f_lims)-1);
% for f=1:length(f_lims)-1
%     Isel=f2>f_lims(f) & f2<=f_lims(f+1);
%     if any(Isel)
%         d2_df(f)=mean(d2(Isel));
%     end
% end
% f_df=f_lims(1:end-1)+(f_lims(2)-f_lims(1))/2;

[f2,Is]=sort(f2);
d2m=pav(d2(Is));%make monotonic

d2_df = interpmon(f2,d2m,f_df);

if doPlot
    subplot(2,2,4)
    plot(f_df,d2_df,'*-r')
    drawnow,
    hold off
end