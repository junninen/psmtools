function dp=psm_flow2size(mf,calib_param)
%
% convert mixing flows to particle size using calibration parameters.
% Calibration parameter are linear fit slope and intercept of calibration
% mixing flows and known sizes
%
% dp=psm_flow2size(mf,calib_param)
%
% 

%Heikki Junninen
%Apr 2012

dp=mf*calib_param(1)+calib_param(2);
