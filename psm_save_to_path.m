function psm_save_to_path()
%
% Adds the path of this function to general matlab path
%

%Heikki Junninen
%Apr 2012


[fPath]=fileparts(which(mfilename));

p=regexp(genpath(fPath),';','split');
%remove .svn paths
pth=[];
for i=1:length(p)
    I=regexp(p{i},'.svn');
    if isempty(I) & ~isempty(p{i})
        pth=[p{i},';',pth]
    end
end
addpath(pth)
savepath