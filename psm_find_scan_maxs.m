function Imx=psm_find_scan_maxs(mixFlow,status,scanSteps)
%
% Find maximas of mixing flows
%
% Imx=H_psm_find_scan_maxs(mixFlow,status)
%
% mixFlow - mixing flows
% status  - status reported by instrumen
% scanSteps - number of steps in each scan
% Imx     - index for maximum flows

% Heikki Junninen
% Apr 2012

if 0
    % if not set value available use measured flow and try to guess where is
    % the maximum flow rate
    Imx= mixFlow(3:end)<mixFlow(2:end-1) & mixFlow(2:end-1)>0.95 & status(2:end-1)==1 &status(1:end-2) &status(3:end);
    Imx=find([false;Imx;false]);
    
    %find max flow data point within a window from the found maximas
    % this is to get rid of duplicates that are few steps apart
    %select the higher one and delete the other
    L=length(Imx);
    Lmx=length(mixFlow);
    for i=1:L
        s=floor(scanSteps/2);
        [~,I]=max(mixFlow(Imx(i)-s:min(Imx(i)+s,Lmx)));
        Imx_new(i)=I+Imx(i)-s-1;
    end
    Imx=unique(Imx_new);
end

%%
%find top platou

Imx=mixFlow(1:end-7)>0.99&...
    mixFlow(2:end-6)>0.99&...
    mixFlow(3:end-5)>0.99&...
    mixFlow(4:end-4)>0.99&...
    mixFlow(5:end-3)>0.99&...
    mixFlow(6:end-2)>0.99&...
    mixFlow(7:end-1)<0.99;
Imx=find([Imx;false;false;false;false;false;false]);

% Imx=mixFlow(1:end-5)>0.&mixFlow(2:end-4)>0.98&mixFlow(3:end-3)>0.98&mixFlow(4:end-2)>0.98&mixFlow(5:end-1)<0.98;
% Imx=find([Imx;false;false;false;false]);